﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnlyFloorPrefab : MonoBehaviour {
    public GameObject maze2DFloor;

    // Use this for initialization
    void Start()
    {
        maze2DFloor = GameObject.Find("Maze2DFloor");
        this.gameObject.transform.SetParent(maze2DFloor.transform);
    }

}
