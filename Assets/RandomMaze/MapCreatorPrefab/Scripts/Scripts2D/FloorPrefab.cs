﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloorPrefab : MonoBehaviour {
    public GameObject maze2D;

	// Use this for initialization
	void Start () {
        maze2D = GameObject.Find("Maze2D");
        this.gameObject.transform.SetParent(maze2D.transform);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
