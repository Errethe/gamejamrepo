﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetZPosition : MonoBehaviour {
    public Vector3 fixedPosition;
    public bool isFixed = false;
    public GameObject maze;

    // Use this for initialization
    void Start () {
        fixedPosition = new Vector3(this.transform.position.x, this.transform.position.y, 0);
        this.transform.position = fixedPosition;
        maze = GameObject.FindGameObjectWithTag("DestroyMaze");
	}
	
	// Update is called once per frame
	void Update () {
        if (isFixed == false)
        {
            fixedPosition = new Vector3(this.transform.position.x, this.transform.position.y, 0);
            this.transform.position = fixedPosition;
            Destroy(maze);
            isFixed = true;
        }
    }
}
