﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class MazeGameManager : MonoBehaviour {

    public Maze mazePrefab;
    private Maze mazeInstance;

    public GameObject[] mazeWallArray;

    public int mazeSizeX = 20;
    public int mazeSizeY = 20;

    public GameObject mazeRotation;

    // Use this for initialization
    void Start () {
          BeginGame();
        mazeInstance.gameObject.transform.rotation = mazeRotation.transform.rotation;
    }
	
	// Update is called once per frame
	void Update () {
       
    }

  
    public void BeginGame()
    {
        System.Array.Clear(mazeWallArray, 0, mazeWallArray.Length);
        
        mazeInstance = Instantiate(mazePrefab) as Maze;
        
            mazeInstance.GenerateInstantly();
            mazeWallArray = GameObject.FindGameObjectsWithTag("MazeWall");
        
       
    }
    public void ExitButton()
    {
        Application.Quit();
    }
}
