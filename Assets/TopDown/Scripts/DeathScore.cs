﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DeathScore : MonoBehaviour
{
    public Text score;
    private float scoreCount;

    // Use this for initialization
    void Start ()
    {
        
    }
	
	// Update is called once per frame
	void Update ()
    {
        score.text = "Your Score: " + Mathf.Round(scoreCount);
    }

    public void DeadScore(float pointsToAdd)
    {
        scoreCount = pointsToAdd;
    }

}
