﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletController : MonoBehaviour
{
    public float speed;

    private Vector3 lenghtBullet;

    public float timeToDestroy;
    
	// Use this for initialization
	void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
        transform.Translate(Vector2.left * speed * Time.deltaTime);

        Object.Destroy(gameObject, timeToDestroy);
    }
}
