﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHealthManager : MonoBehaviour
{
    public float MaxHealth;
    public float CurrentHealth;

    private PlayerStats thePlayerStats;
    public int expToGive;

    // Use this for initialization
    void Start ()
    {
        CurrentHealth = MaxHealth;
        thePlayerStats = FindObjectOfType<PlayerStats>();
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (CurrentHealth <= 0)
        {
            Destroy(gameObject);
            thePlayerStats.AddExperience(expToGive);
        }

    }

    public void HurtEnemy(float damageToGive)
    {
        CurrentHealth -= damageToGive;
    }

    public void SetMaxHealth()
    {
        CurrentHealth = MaxHealth;
    }

}
