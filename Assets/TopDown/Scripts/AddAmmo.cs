﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddAmmo : MonoBehaviour
{
    public int ammo;
    public int addAmmo;

	// Use this for initialization
	void Start ()
    {
        
    }
	
	// Update is called once per frame
	void Update ()
    {
        
	}

    void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.name == "Player" && ammo > 0)
        {
            ammo -= 100;
            other.gameObject.transform.GetChild(0).GetComponent<GunController>().ammo += addAmmo;
        }
    }
}
