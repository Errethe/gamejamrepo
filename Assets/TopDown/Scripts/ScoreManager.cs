﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour {

    public Text scoreText;
    public Text hiScoreText;

    public float scoreCount;
    public float hiScoreCount;

    public float pointsPerSecond;

    public bool scoreIncreasing;

    public float Scoree;
    private DeathScore theDeathScore;

    // Use this for initialization
    void Start ()
    {
        if (PlayerPrefs.HasKey("HighScore"))
        {
            hiScoreCount = PlayerPrefs.GetFloat("HighScore");
        }
        theDeathScore = FindObjectOfType<DeathScore>();
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (scoreIncreasing)
        {
            scoreCount += pointsPerSecond * Time.deltaTime;  
        }

        if (scoreCount > hiScoreCount)
        {
            hiScoreCount = scoreCount;
            PlayerPrefs.SetFloat("HighScore", hiScoreCount);
            PlayerPrefs.Save();
        }
        scoreText.text = "Score: " + Mathf.Round (scoreCount);
        hiScoreText.text = "High Score: " + Mathf.Round (hiScoreCount);

        if (Scoree < scoreCount)
        {
            Scoree = scoreCount;
            theDeathScore.DeadScore(Scoree);
        }               //Death score after fall
    }


    public void AddScore(int pointsToAdd)
    {
        scoreCount += pointsToAdd;
    }   //Corn Points



}
