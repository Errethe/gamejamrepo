﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HurtEnemy : MonoBehaviour
{

    public float damageToGive;
    public float currentDamage;

    private PlayerStats thePS;

	// Use this for initialization
	void Start ()
    {
        thePS = FindObjectOfType<PlayerStats>();
	}
	
	// Update is called once per frame
	void Update ()
    {
        currentDamage = damageToGive + thePS.currentAttack;
	}
    void OnCollisionEnter2D(Collision2D other)
    {
        if(other.gameObject.tag == "Enemy")
        {
            currentDamage = damageToGive + thePS.currentAttack;
            other.gameObject.GetComponent<EnemyHealthManager>().HurtEnemy(currentDamage);
            
            Destroy(gameObject);
        }
    }
}
